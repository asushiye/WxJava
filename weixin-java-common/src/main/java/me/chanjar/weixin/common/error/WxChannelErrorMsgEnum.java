package me.chanjar.weixin.common.error;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * TODO
 *
 * @author liuwei.wang
 * @date 2024/3/6 15:55
 */
public enum WxChannelErrorMsgEnum {
    CODE_0(0,"请求成功"),
    CODE_1(-1,"系统繁忙，此时请开发者稍候再试"),
    CODE_40001(40001,"获取 access_token 时 AppSecret 错误，或者 access_token 无效。请开发者认真检查 AppSecret 的正确性"),
    CODE_40003(40003,"请检查 openid 的正确性"),
    CODE_40013(40013,"请检查 appid 的正确性，避免异常字符，注意大小写"),
    CODE_40066(40066,"请检查API的URL是否与文档一致"),
    CODE_41001(41001,"缺少 access_token 参数"),
    CODE_41002(41002,"请检查URL参数中是否有 ?appid="),
    CODE_41018(41018,"请检查POST json中是否包含component_ appid宇段"),
    CODE_42001(42001,"获取新的 access_token，https://mmbizurl.cn/s/JtxxFh33r"),
    CODE_43002(43002,"请检查发起API请求的Method是否为POST"),
    CODE_43003(43003,"请使用HTTPS方式清求，不要使用HTTP方式"),
    CODE_44002(44002,"POST 的数据包为空"),
    CODE_45002(45002,"请对数据进行压缩"),
    CODE_45009(45009,"查看调用次数是否符合预期，可通过get_api_quota接口获取每天的调用quota；用完后可通过clear_quota进行清空"),
    CODE_45011(45011,"命中每分钟的频率限制"),
    CODE_45035(45035,"需要登录 channels.weixin.qq.com/shop 配置IP白名单"),
    CODE_47001(47001,"解析 JSON/XML 内容错误"),
    CODE_48001(48001,"没有该接口权限"),
    CODE_48004(48004,"接口被禁用"),
    CODE_50001(50001,"请找用户获取该api授权"),
    CODE_50002(50002,"请检查封禁原因"),
    CODE_61004(61004,"需要登陆 channels.weixin.qq.com/shop 配置IP白名单"),
    CODE_61007(61007,"请检查第三方平台服务商检查已获取的授权集"),
    CODE_10080000(10080000,"需要登陆 channels.weixin.qq.com/shop 继续完成注销"),
    CODE_10080001(10080001,"账号已注销"),
    CODE_89503(89503,"此次调用需要管理员确认，请耐心等候");

    private final int code;
    private final String msg;

    private WxChannelErrorMsgEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    static final Map<Integer, String> valueMap = Maps.newHashMap();

    public static String findMsgByCode(int code) {
        return (String)valueMap.getOrDefault(code, null);
    }

    static {
        WxChannelErrorMsgEnum[] var0 = values();
        int var1 = var0.length;
        for(int var2 = 0; var2 < var1; ++var2) {
            WxChannelErrorMsgEnum value = var0[var2];
            valueMap.put(value.code, value.msg);
        }

    }

}
